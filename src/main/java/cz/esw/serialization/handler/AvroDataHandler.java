package cz.esw.serialization.handler;

import cz.esw.serialization.ResultConsumer;
import cz.esw.serialization.avro.*;
import cz.esw.serialization.json.DataType;
import org.apache.avro.io.*;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;

import java.io.*;
import java.util.*;

/**
 * @author Marek Cuchý (CVUT)
 */
public class AvroDataHandler implements DataHandler {

    private final InputStream is;
    private final OutputStream os;

    protected Map<Integer, ADataset> datasets;
    private ADatasets aDatasets;

    public AvroDataHandler(InputStream is, OutputStream os) {
        this.is = is;
        this.os = os;
    }

    @Override
    public void start() {
        datasets = new HashMap<>();
        aDatasets = ADatasets.newBuilder()
                .setItems(new ArrayList<>())
                .build();
    }

    @Override
    public void handleNewDataset(int datasetId, long timestamp, String measurerName) {
        ADataset dataset = ADataset.newBuilder()
                .setInfo(AMeasurementInfo.newBuilder()
                        .setId(datasetId)
                        .setTimestamp(timestamp)
                        .setMeasurerName(measurerName)
                        .build())
                .setRecordsPairs(new ArrayList<>(ADatasetType.values().length))
                .build();
        datasets.put(datasetId, dataset);
        aDatasets.getItems().add(dataset);
    }

    @Override
    public void handleValue(int datasetId, DataType type, double value) {
        ADataset dataset = datasets.get(datasetId);
        if (dataset == null) {
            throw new IllegalArgumentException("Dataset with id " + datasetId + " not initialized.");
        }
        int typeId = type.ordinal();
        if (typeId >= dataset.getRecordsPairs().size()) {
            // fill array
            for (int i = dataset.getRecordsPairs().size(); i <= typeId; i++) {
                dataset.getRecordsPairs().add(null);
            }
        }
        ARecordsPair recordsPair = dataset.getRecordsPairs().get(typeId);
        if (recordsPair == null) {
            dataset.getRecordsPairs().set(typeId, recordsPair = ARecordsPair.newBuilder()
                    .setType(ADatasetType.valueOf(type.toString()))
                    .setRecords(new ArrayList<>())
                    .build());
        }
        recordsPair.getRecords().add(value);
    }

    private AResults getResults() throws IOException {
        DatumWriter<ADatasets> datumWriter = new SpecificDatumWriter<>(ADatasets.class);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BinaryEncoder encoder = EncoderFactory.get().binaryEncoder(baos, null);
        datumWriter.write(aDatasets, encoder);
        encoder.flush();

        int messageSize = baos.size();
        os.write(Integer.toString(messageSize).getBytes());
        os.write(0);

        baos.writeTo(os);

        BinaryDecoder bd = DecoderFactory.get().binaryDecoder(is, null);
        DatumReader<AResults> datumReader = new SpecificDatumReader<>(AResults.class);
        AResults results = datumReader.read(null, bd);
        return results;
    }

    @Override
    public void getResults(ResultConsumer consumer) {
        AResults results;
        try {
            results = getResults();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        for (AResult result : results.getItems()) {
            AMeasurementInfo info = result.getInfo();
            consumer.acceptMeasurementInfo(info.getId(), info.getTimestamp(), info.getMeasurerName().toString());
            result.getResultPairs().forEach(resultPair -> {
                consumer.acceptResult(DataType.valueOf(resultPair.getType().toString()), resultPair.getAverage());
            });
        }
    }
}
