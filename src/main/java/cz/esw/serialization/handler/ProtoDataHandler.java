package cz.esw.serialization.handler;

import cz.esw.serialization.ResultConsumer;
import cz.esw.serialization.json.DataType;
import cz.esw.serialization.proto.*;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

/**
 * @author Marek Cuchý (CVUT)
 */
public class ProtoDataHandler implements DataHandler {
    private final InputStream is;
    private final OutputStream os;

    private Map<Integer, Dataset> datasets = new HashMap<>();

    private static class Dataset {
        private PMeasurementsInfo info;
        private Map<DataType, List<Double>> types = new HashMap<>();
    }

    public ProtoDataHandler(InputStream is, OutputStream os) {
        this.is = is;
        this.os = os;
    }

    @Override
    public void start() {

    }

    @Override
    public void handleNewDataset(int datasetId, long timestamp, String measurerName) {
        Dataset dataset = new Dataset();
        dataset.info = PMeasurementsInfo.newBuilder()
                .setId(datasetId)
                .setTimestamp(timestamp)
                .setMeasurerName(measurerName)
                .build();
        datasets.put(datasetId, dataset);
    }

    @Override
    public void handleValue(int datasetId, DataType type, double value) {
        Dataset dataset = datasets.get(datasetId);
        List<Double> values = dataset.types.computeIfAbsent(type, (t) -> new ArrayList<>());
        values.add(value);
    }

    private String getTypeKey(DataType type) {
        return Integer.toString(type.ordinal() + 1);
    }

    private void sendMessageSize(int size) throws IOException {
        DataOutputStream dos = new DataOutputStream(os);
        dos.writeBytes(Integer.toString(size));
        dos.writeByte(0);
    }

    @Override
    public void getResults(ResultConsumer consumer) throws IOException {
        PDatasets.Builder datasetsBuilder = PDatasets.newBuilder();
        for (Map.Entry<Integer, Dataset> datasetEntry : datasets.entrySet()) {
            PDataset.Builder datasetBuilder = PDataset.newBuilder()
                    .setInfo(datasetEntry.getValue().info);
            for (Map.Entry<DataType, List<Double>> typeListEntry : datasetEntry.getValue().types.entrySet()) {
//                System.out.printf("%s %s\n", typeListEntry.getKey(), getTypeKey(typeListEntry.getKey()));
                datasetBuilder.putRecords(
                        getTypeKey(typeListEntry.getKey()),
                        PList.newBuilder().addAllItem(typeListEntry.getValue()).build()
                );
            }
            datasetsBuilder.addAllOfThem(datasetBuilder);
        }

        int messageSize = datasetsBuilder.build().getSerializedSize();
        sendMessageSize(messageSize); // your implementation
        datasetsBuilder.build().writeTo(os);
        os.write(0);
        os.flush();

        PResults results = PResults.parseFrom(is);

        for (PResult result : results.getSetList()) {
            PMeasurementsInfo info = result.getInfo();
            consumer.acceptMeasurementInfo(info.getId(), info.getTimestamp(), info.getMeasurerName());

            Map<String, Double> myMap = result.getAveragesMap();
            System.out.println(myMap.get("1"));
            System.out.println(myMap.get("2"));
            System.out.println(myMap.get("3"));
            System.out.println();
            consumer.acceptResult(DataType.DOWNLOAD, myMap.get("1"));
            consumer.acceptResult(DataType.UPLOAD, myMap.get("2"));
            consumer.acceptResult(DataType.PING, myMap.get("3"));
        }


    }
}
