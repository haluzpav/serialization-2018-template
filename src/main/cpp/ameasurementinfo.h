/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef AMEASUREMENTINFO_H_3143018629__H_
#define AMEASUREMENTINFO_H_3143018629__H_


#include <sstream>
#include "boost/any.hpp"
#include "avro/Specific.hh"
#include "avro/Encoder.hh"
#include "avro/Decoder.hh"

namespace eswa {
struct AMeasurementInfo {
    int32_t id;
    int64_t timestamp;
    std::string measurerName;
    AMeasurementInfo() :
        id(int32_t()),
        timestamp(int64_t()),
        measurerName(std::string())
        { }
};

enum ADatasetType {
    DOWNLOAD,
    UPLOAD,
    PING,
};

struct ARecordsPair {
    ADatasetType type;
    std::vector<double > records;
    ARecordsPair() :
        type(ADatasetType()),
        records(std::vector<double >())
        { }
};

struct _measurements_avsc_Union__0__ {
private:
    size_t idx_;
    boost::any value_;
public:
    size_t idx() const { return idx_; }
    bool is_null() const {
        return (idx_ == 0);
    }
    void set_null() {
        idx_ = 0;
        value_ = boost::any();
    }
    ARecordsPair get_ARecordsPair() const;
    void set_ARecordsPair(const ARecordsPair& v);
    _measurements_avsc_Union__0__();
};

struct ADataset {
    AMeasurementInfo info;
    std::vector<_measurements_avsc_Union__0__ > recordsPairs;
    ADataset() :
        info(AMeasurementInfo()),
        recordsPairs(std::vector<_measurements_avsc_Union__0__ >())
        { }
};

struct AResultPair {
    ADatasetType type;
    double average;
    AResultPair() :
        type(ADatasetType()),
        average(double())
        { }
};

struct AResult {
    AMeasurementInfo info;
    std::vector<AResultPair > resultPairs;
    AResult() :
        info(AMeasurementInfo()),
        resultPairs(std::vector<AResultPair >())
        { }
};

struct ADatasets {
    std::vector<ADataset > items;
    ADatasets() :
        items(std::vector<ADataset >())
        { }
};

struct AResults {
    std::vector<AResult > items;
    AResults() :
        items(std::vector<AResult >())
        { }
};

struct _measurements_avsc_Union__1__ {
private:
    size_t idx_;
    boost::any value_;
public:
    size_t idx() const { return idx_; }
    AMeasurementInfo get_AMeasurementInfo() const;
    void set_AMeasurementInfo(const AMeasurementInfo& v);
    ADatasetType get_ADatasetType() const;
    void set_ADatasetType(const ADatasetType& v);
    ARecordsPair get_ARecordsPair() const;
    void set_ARecordsPair(const ARecordsPair& v);
    ADataset get_ADataset() const;
    void set_ADataset(const ADataset& v);
    AResultPair get_AResultPair() const;
    void set_AResultPair(const AResultPair& v);
    AResult get_AResult() const;
    void set_AResult(const AResult& v);
    ADatasets get_ADatasets() const;
    void set_ADatasets(const ADatasets& v);
    AResults get_AResults() const;
    void set_AResults(const AResults& v);
    _measurements_avsc_Union__1__();
};

inline
ARecordsPair _measurements_avsc_Union__0__::get_ARecordsPair() const {
    if (idx_ != 1) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<ARecordsPair >(value_);
}

inline
void _measurements_avsc_Union__0__::set_ARecordsPair(const ARecordsPair& v) {
    idx_ = 1;
    value_ = v;
}

inline
AMeasurementInfo _measurements_avsc_Union__1__::get_AMeasurementInfo() const {
    if (idx_ != 0) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<AMeasurementInfo >(value_);
}

inline
void _measurements_avsc_Union__1__::set_AMeasurementInfo(const AMeasurementInfo& v) {
    idx_ = 0;
    value_ = v;
}

inline
ADatasetType _measurements_avsc_Union__1__::get_ADatasetType() const {
    if (idx_ != 1) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<ADatasetType >(value_);
}

inline
void _measurements_avsc_Union__1__::set_ADatasetType(const ADatasetType& v) {
    idx_ = 1;
    value_ = v;
}

inline
ARecordsPair _measurements_avsc_Union__1__::get_ARecordsPair() const {
    if (idx_ != 2) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<ARecordsPair >(value_);
}

inline
void _measurements_avsc_Union__1__::set_ARecordsPair(const ARecordsPair& v) {
    idx_ = 2;
    value_ = v;
}

inline
ADataset _measurements_avsc_Union__1__::get_ADataset() const {
    if (idx_ != 3) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<ADataset >(value_);
}

inline
void _measurements_avsc_Union__1__::set_ADataset(const ADataset& v) {
    idx_ = 3;
    value_ = v;
}

inline
AResultPair _measurements_avsc_Union__1__::get_AResultPair() const {
    if (idx_ != 4) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<AResultPair >(value_);
}

inline
void _measurements_avsc_Union__1__::set_AResultPair(const AResultPair& v) {
    idx_ = 4;
    value_ = v;
}

inline
AResult _measurements_avsc_Union__1__::get_AResult() const {
    if (idx_ != 5) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<AResult >(value_);
}

inline
void _measurements_avsc_Union__1__::set_AResult(const AResult& v) {
    idx_ = 5;
    value_ = v;
}

inline
ADatasets _measurements_avsc_Union__1__::get_ADatasets() const {
    if (idx_ != 6) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<ADatasets >(value_);
}

inline
void _measurements_avsc_Union__1__::set_ADatasets(const ADatasets& v) {
    idx_ = 6;
    value_ = v;
}

inline
AResults _measurements_avsc_Union__1__::get_AResults() const {
    if (idx_ != 7) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<AResults >(value_);
}

inline
void _measurements_avsc_Union__1__::set_AResults(const AResults& v) {
    idx_ = 7;
    value_ = v;
}

inline _measurements_avsc_Union__0__::_measurements_avsc_Union__0__() : idx_(0) { }
inline _measurements_avsc_Union__1__::_measurements_avsc_Union__1__() : idx_(0), value_(AMeasurementInfo()) { }
}
namespace avro {
template<> struct codec_traits<eswa::AMeasurementInfo> {
    static void encode(Encoder& e, const eswa::AMeasurementInfo& v) {
        avro::encode(e, v.id);
        avro::encode(e, v.timestamp);
        avro::encode(e, v.measurerName);
    }
    static void decode(Decoder& d, eswa::AMeasurementInfo& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.id);
                    break;
                case 1:
                    avro::decode(d, v.timestamp);
                    break;
                case 2:
                    avro::decode(d, v.measurerName);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.id);
            avro::decode(d, v.timestamp);
            avro::decode(d, v.measurerName);
        }
    }
};

template<> struct codec_traits<eswa::ADatasetType> {
    static void encode(Encoder& e, eswa::ADatasetType v) {
		if (v < eswa::DOWNLOAD || v > eswa::PING)
		{
			std::ostringstream error;
			error << "enum value " << v << " is out of bound for eswa::ADatasetType and cannot be encoded";
			throw avro::Exception(error.str());
		}
        e.encodeEnum(v);
    }
    static void decode(Decoder& d, eswa::ADatasetType& v) {
		size_t index = d.decodeEnum();
		if (index < eswa::DOWNLOAD || index > eswa::PING)
		{
			std::ostringstream error;
			error << "enum value " << index << " is out of bound for eswa::ADatasetType and cannot be decoded";
			throw avro::Exception(error.str());
		}
        v = static_cast<eswa::ADatasetType>(index);
    }
};

template<> struct codec_traits<eswa::ARecordsPair> {
    static void encode(Encoder& e, const eswa::ARecordsPair& v) {
        avro::encode(e, v.type);
        avro::encode(e, v.records);
    }
    static void decode(Decoder& d, eswa::ARecordsPair& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.type);
                    break;
                case 1:
                    avro::decode(d, v.records);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.type);
            avro::decode(d, v.records);
        }
    }
};

template<> struct codec_traits<eswa::_measurements_avsc_Union__0__> {
    static void encode(Encoder& e, eswa::_measurements_avsc_Union__0__ v) {
        e.encodeUnionIndex(v.idx());
        switch (v.idx()) {
        case 0:
            e.encodeNull();
            break;
        case 1:
            avro::encode(e, v.get_ARecordsPair());
            break;
        }
    }
    static void decode(Decoder& d, eswa::_measurements_avsc_Union__0__& v) {
        size_t n = d.decodeUnionIndex();
        if (n >= 2) { throw avro::Exception("Union index too big"); }
        switch (n) {
        case 0:
            d.decodeNull();
            v.set_null();
            break;
        case 1:
            {
                eswa::ARecordsPair vv;
                avro::decode(d, vv);
                v.set_ARecordsPair(vv);
            }
            break;
        }
    }
};

template<> struct codec_traits<eswa::ADataset> {
    static void encode(Encoder& e, const eswa::ADataset& v) {
        avro::encode(e, v.info);
        avro::encode(e, v.recordsPairs);
    }
    static void decode(Decoder& d, eswa::ADataset& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.info);
                    break;
                case 1:
                    avro::decode(d, v.recordsPairs);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.info);
            avro::decode(d, v.recordsPairs);
        }
    }
};

template<> struct codec_traits<eswa::AResultPair> {
    static void encode(Encoder& e, const eswa::AResultPair& v) {
        avro::encode(e, v.type);
        avro::encode(e, v.average);
    }
    static void decode(Decoder& d, eswa::AResultPair& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.type);
                    break;
                case 1:
                    avro::decode(d, v.average);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.type);
            avro::decode(d, v.average);
        }
    }
};

template<> struct codec_traits<eswa::AResult> {
    static void encode(Encoder& e, const eswa::AResult& v) {
        avro::encode(e, v.info);
        avro::encode(e, v.resultPairs);
    }
    static void decode(Decoder& d, eswa::AResult& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.info);
                    break;
                case 1:
                    avro::decode(d, v.resultPairs);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.info);
            avro::decode(d, v.resultPairs);
        }
    }
};

template<> struct codec_traits<eswa::ADatasets> {
    static void encode(Encoder& e, const eswa::ADatasets& v) {
        avro::encode(e, v.items);
    }
    static void decode(Decoder& d, eswa::ADatasets& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.items);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.items);
        }
    }
};

template<> struct codec_traits<eswa::AResults> {
    static void encode(Encoder& e, const eswa::AResults& v) {
        avro::encode(e, v.items);
    }
    static void decode(Decoder& d, eswa::AResults& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.items);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.items);
        }
    }
};

template<> struct codec_traits<eswa::_measurements_avsc_Union__1__> {
    static void encode(Encoder& e, eswa::_measurements_avsc_Union__1__ v) {
        e.encodeUnionIndex(v.idx());
        switch (v.idx()) {
        case 0:
            avro::encode(e, v.get_AMeasurementInfo());
            break;
        case 1:
            avro::encode(e, v.get_ADatasetType());
            break;
        case 2:
            avro::encode(e, v.get_ARecordsPair());
            break;
        case 3:
            avro::encode(e, v.get_ADataset());
            break;
        case 4:
            avro::encode(e, v.get_AResultPair());
            break;
        case 5:
            avro::encode(e, v.get_AResult());
            break;
        case 6:
            avro::encode(e, v.get_ADatasets());
            break;
        case 7:
            avro::encode(e, v.get_AResults());
            break;
        }
    }
    static void decode(Decoder& d, eswa::_measurements_avsc_Union__1__& v) {
        size_t n = d.decodeUnionIndex();
        if (n >= 8) { throw avro::Exception("Union index too big"); }
        switch (n) {
        case 0:
            {
                eswa::AMeasurementInfo vv;
                avro::decode(d, vv);
                v.set_AMeasurementInfo(vv);
            }
            break;
        case 1:
            {
                eswa::ADatasetType vv;
                avro::decode(d, vv);
                v.set_ADatasetType(vv);
            }
            break;
        case 2:
            {
                eswa::ARecordsPair vv;
                avro::decode(d, vv);
                v.set_ARecordsPair(vv);
            }
            break;
        case 3:
            {
                eswa::ADataset vv;
                avro::decode(d, vv);
                v.set_ADataset(vv);
            }
            break;
        case 4:
            {
                eswa::AResultPair vv;
                avro::decode(d, vv);
                v.set_AResultPair(vv);
            }
            break;
        case 5:
            {
                eswa::AResult vv;
                avro::decode(d, vv);
                v.set_AResult(vv);
            }
            break;
        case 6:
            {
                eswa::ADatasets vv;
                avro::decode(d, vv);
                v.set_ADatasets(vv);
            }
            break;
        case 7:
            {
                eswa::AResults vv;
                avro::decode(d, vv);
                v.set_AResults(vv);
            }
            break;
        }
    }
};

}
#endif
