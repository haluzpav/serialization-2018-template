#include <iostream>

#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <cstring>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#include "measurements.pb.h"

#include <jsoncpp/json/json.h>


#include "measurementinfo.h"
#include "dataset.h"
#include "result.h"

#include <iostream>
#include <istream>
#include <streambuf>
#include <string>
#include <fstream>

#include <boost/interprocess/streams/bufferstream.hpp>
using namespace boost::interprocess;

#include <boost/asio.hpp>
#include <boost/lexical_cast.hpp>

#include "ameasurementinfo.h"
#include <avro/ValidSchema.hh>
#include <avro/Compiler.hh>
#include <avro/DataFile.hh>
#include <avro/Stream.hh>
#include <avro/Encoder.hh>

using namespace std;
using boost::asio::ip::tcp;

void processJSON(tcp::iostream& stream){
    Json::Value val;
    Json::Reader reader;

    std::vector<Dataset> datasets;
    std::vector<Result> results;

    /* Read json string from the stream */
    string s;
    getline(stream, s, '\0');

    /* Parse string */
    reader.parse(s, val);

    datasets.clear();
    results.clear();
    for (int i = 0; i < val.size(); i++) {
        datasets.emplace_back();
        datasets[i].Deserialize(val[i]);
        /* Calculate averages */
        results.emplace_back(datasets[i].getInfo(), datasets[i].getRecords());
    }

    /* Create output JSON structure */
    Json::Value out;
//    Json::FastWriter writer;
    Json::StyledWriter writer;
    for (int i = 0; i < results.size(); i++) {
        Json::Value result;
        results[i].Serialize(result);
        out[i] = result;
    }

    /* Send the result back */
    std::string output = writer.write(out);
    stream << output;
    cout << output;
}

void processAvro(tcp::iostream& stream){
    eswa::ADatasets datasets;
    eswa::AResults results;

    {
        std::string messageSizeStr;
        getline(stream, messageSizeStr, '\0');
        int messageSize = std::stoi(messageSizeStr);

        char *buffer = new char[messageSize];
        stream.read(buffer, messageSize);

        auto in = avro::memoryInputStream((uint8_t *) buffer, messageSize);
        auto d = avro::binaryDecoder();
        d->init(*in);
        avro::decode(*d, datasets);
    }

    for (const auto &dataset : datasets.items) {
        eswa::AResult result;
        result.info = dataset.info;
        for (const auto &recordsPairOptional : dataset.recordsPairs) {
            if (recordsPairOptional.is_null()) {
                continue;
            }
            eswa::ARecordsPair recordsPair = recordsPairOptional.get_ARecordsPair();
            double sum = 0;
            for (auto record : recordsPair.records) {
                sum += record;
            }
            double avg = sum / recordsPair.records.size();
            eswa::AResultPair resultPair;
            resultPair.type = recordsPair.type;
            resultPair.average = avg;
            result.resultPairs.push_back(resultPair);
        }
        results.items.push_back(result);
    }

    {
        auto oStream = avro::ostreamOutputStream(stream);
        auto e = avro::binaryEncoder();
        e->init(*oStream);
        avro::encode(*e, results);
        e->flush();
    }
}

void processProtobuf(tcp::iostream& stream){
    string messageSizeStr;
    getline(stream, messageSizeStr, '\0');
    int messageSize = std::stoi(messageSizeStr);

    char *buffer = new char[messageSize];
    stream.read(buffer, messageSize);

    string stringBuffer = std::string(buffer, messageSize);

    esw::PDatasets datasets;
    esw::PResults results;
    
    if (!datasets.ParseFromString(stringBuffer)) {
        printf("shit\n");
        return;
    }

    for(esw::PDataset dataset : datasets.allofthem()) {
        esw::PResult* result = results.add_set();
        esw::PMeasurementsInfo* info = new esw::PMeasurementsInfo();
        info->set_id(dataset.info().id());
        info->set_timestamp(dataset.info().timestamp());
        info->set_measurername(dataset.info().measurername());
        result->set_allocated_info(info);

        for (auto enumType : dataset.records()) {
            auto plist = enumType.second;
            int n = 0;
            double sum = 0;
            for(double item : plist.item()) {
                n++;
                sum += item;
            }
            (*result->mutable_averages())[enumType.first] = sum / n;
        }
    }

    stream << results.SerializeAsString();
}


int main(int argc, char *argv[]) {

    if (argc != 3) {
        cout << "Error: two arguments required - ./server  <port> <protocol>" << endl;
        return 1;
    }



    // unsigned short int port = 12345;
    unsigned short int port = atoi(argv[1]);

    // std::string protocol = "json";
    std::string protocol(argv[2]);
    try {
        boost::asio::io_service io_service;

        tcp::endpoint endpoint(tcp::v4(), port);
        tcp::acceptor acceptor(io_service, endpoint);

        while (true) {
            tcp::iostream stream;
            boost::system::error_code ec;
            acceptor.accept(*stream.rdbuf(), ec);

            if(protocol == "json"){
                processJSON(stream);
            }else if(protocol == "avro"){
                processAvro(stream);
            }else if(protocol == "proto"){
                processProtobuf(stream);
            }else{
                throw std::logic_error("Protocol not yet implemented");
            }

        }

    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}
